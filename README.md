# AGLS

The AGLS module extends the [Metatag module](https://www.drupal.org/project/metatag) to add [AGLS Metadata Standard](http://agls.gov.au/) tags, a set of descriptive properties to improve visibility and availability of online resources.

## Meta tags

* AGLSTERMS.act
* AGLSTERMS.aggregationLevel
* AGLSTERMS.availability
* AGLSTERMS.case
* AGLSTERMS.category
* AGLSTERMS.dateLicenced
* AGLSTERMS.documentType
* AGLSTERMS.function
* AGLSTERMS.jurisdiction
* AGLSTERMS.mandate
* AGLSTERMS.protectiveMarking
* AGLSTERMS.regulation
* AGLSTERMS.rightsHolder
* AGLSTERMS.spatial
* AGLSTERMS.temporal

## Meta tag obligation in AGLS compliance

The AGLS standard states that metadata properties fall into one of four
obligation categories:

* Mandatory
  * dcterms:creator
  * dcterms:title
  * dcterms:date (a related term may be substituted)
* Conditional
  * aglsterms:availability (mandatory for offline resources)
  * dcterms:identifier (mandatory for online resources)
  * dcterms:publisher (mandatory for information resources - optional for
   descriptions of services)
* Recommended
  * aglsterms:function (if dcterms:subject is not used)
  * dcterms:description
  * dcterms:language (where the language of the resource is not English)
  * dcterms:subject (if aglsterms:function is not used)
  * dcterms:type
* Optional
  * All other properties are optional.

## References and further reading

* [AGLS Metadata Standard](http://agls.gov.au/)
* [AGLS Reference Description](https://agls.gov.au/pdf/AGLS%20Metadata%20Standard%20Part%201%20Reference%20Description.pdf)
* [AGLS Usage Guide](https://agls.gov.au/pdf/AGLS%20Metadata%20Standard%20Part%202%20Usage%20Guide.pdf)
* [Guide to expressing AGLS metadata in XML](https://agls.gov.au/pdf/Guide%20to%20expressing%20AGLS%20metadata%20in%20XML%20v1.0.pdf)
* [Guide to expressing AGLS metadata in RDF](https://agls.gov.au/pdf/Guide%20to%20expressing%20AGLS%20metadata%20in%20RDF%20v1.0.pdf)

## Credits / Contact

Currently maintained by [Damien McKenna](https://www.drupal.org/u/damienmckenna). Drupal 8 port written by [Ben Dougherty](https://www.drupal.org/u/benjy). Originally written by [Kim Pepper](https://www.drupal.org/u/kim.pepper), with sponsorship by [Previous Next Pty Ltd](https://www.previousnext.com.au), the [Australian
Law Reform Commission](https://www.alrc.gov.au/) and the [Australian Department of Families, Housing,
Community Services and Indigenous Affairs](https://www.dss.gov.au/).

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the [project's issue queue](https://www.drupal.org/project/issues/agls).
