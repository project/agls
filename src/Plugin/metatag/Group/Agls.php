<?php

namespace Drupal\agls\Plugin\metatag\Group;

use Drupal\metatag\Plugin\metatag\Group\GroupBase;

/**
 * Provides a Metatag Group structure for the new Metatag plugins to use.
 *
 * @MetatagGroup(
 *   id = "agls",
 *   label = @Translation("Agls"),
 *   description = @Translation("The AGLS Metadata Standard provides a set of metadata properties and associated usage guidelines to improve the visibility, manageability and interoperability of online information and services."),
 *   weight = 0
 * )
 */
class Agls extends GroupBase {

}
