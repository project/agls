<?php

/**
 * @file
 * Contains agls.module.
 */
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function agls_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the agls module.
    case 'help.page.agls':
      if (file_exists(__DIR__ . "/README.md")) {
        $content = file_get_contents(__DIR__ . "/README.md");
        if ($content !== FALSE) {
          // If the Markdown module is not available, just output the text
          // wrapped in a PRE tag.
          if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
            return '<pre>' . $content . '</pre>';
          }
          // Use the Markdown filter.
          else {
            $filter_manager = \Drupal::service('plugin.manager.filter');
            $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
            $config = ['settings' => $settings];
            $filter = $filter_manager->createInstance('markdown', $config);
            return $filter->process($text, 'en');
          }
        }
      }
    default:
      return [];
  }
}

/**
 * Implements hook_metatags_attachments_alter().
 */
function agls_metatags_attachments_alter(array &$metatag_attachments) {
  // These two meta tags are required by the AGLS standard.
  // @see https://agls.gov.au/documents/html5-validation.html
  $metatag_attachments['#attached']['html_head'][] = [
    [
      '#tag' => 'link',
      '#attributes' => [
        'rel' => 'schema.dcterms',
        'href' => 'http://purl.org/dc/terms/',
      ],
    ],
    'schema.dcterms',
  ];
  $metatag_attachments['#attached']['html_head'][] = [
    [
      '#tag' => 'link',
      '#attributes' => [
        'rel' => 'schema.AGLSTERMS',
        'href' => 'http://www.agls.gov.au/agls/terms/',
      ],
    ],
    'schema.AGLSTERMS',
  ];
}
